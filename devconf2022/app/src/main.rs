#![recursion_limit = "1024"] // Yup, parsing HTML can be tricky!

#[macro_use]
mod my_macros;
mod pages;
mod so_what;

use crate::pages::MenuMessages;
use crate::pages::RootComponent;
use crate::MenuMessages::Home;
use yew::prelude::*;

// Yew gives us some lifecycle features that we can implement as a trait
impl Component for RootComponent {
    type Message = MenuMessages;
    type Properties = (); // we don't have properties from a parent component, think React Props

    //----------------------------------------------------------------------------------------------
    // Called upon component creation, so we set our initial state here
    fn create(_: &Context<Self>) -> Self {
        Self { active_item: Home }
    }

    //----------------------------------------------------------------------------------------------
    // When we get called with a request to update our state, let's do that!
    fn update(&mut self, _: &Context<Self>, msg: Self::Message) -> bool {
        self.active_item = msg;
        true // signals we want to rerender the component
    }

    //----------------------------------------------------------------------------------------------
    // What does this thing actually look like, huh?
    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();

        html! {
            <>
                {self.render_main_menu(link)}
                {self.switch_to_selected_content(self.active_item)}
            </>
        }
    }
}

//--------------------------------------------------------------------------------------------------
fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    log::info!("We have liftoff!");
    yew::start_app::<RootComponent>();
    log::info!("Look ma, async for free! Whoo whoo!");
}
