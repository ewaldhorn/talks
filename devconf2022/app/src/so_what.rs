use crate::RootComponent;
use yew::prelude::*;

pub struct Counter {
    value: i64,
}

pub enum Msg {
    Increment,
    Decrement,
}

impl Component for Counter {
    type Message = Msg;
    type Properties = ();

    fn create(_: &Context<Self>) -> Self {
        Self { value: 0 }
    }

    fn update(&mut self, _: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::Increment => self.value += 1,
            Msg::Decrement => self.value -= 1,
        }
        true
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();

        html! {
            <div>
                <p style="font-size: 32pt;">{self.value}</p>
              <button onclick={link.callback(|_| Msg::Increment)} style="font-size: 32pt; width: 50px;">
                {"+"}
              </button>
              {" "}
              <button onclick={link.callback(|_| Msg::Decrement)} style="font-size: 32pt; width: 50px;">
                {"-"}
              </button>
            </div>
        }
    }
}

impl RootComponent {
    pub fn render_so_what(&self) -> Html {
        return html! {
            <>
                {page_header!("Ok, but, so what...", "What's the point?")}
                {page_content!(html!{
                    <>
                        <p style="text-align: center;">
                            {"Stuff like this, but without knowing any JavaScript!"}
                            <br/>
                            <br/>
                            <Counter />
                        </p>
                    </>
                })}
            </>
        };
    }
}
