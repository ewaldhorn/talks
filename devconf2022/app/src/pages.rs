use yew::html::Scope;
use yew::prelude::*;

//--------------------------------------------------------------------------------------------------
// We need to be able to send some messages, hence enums!
// Remember to implement Copy & Clone, to prevent the owner changing...
// Using the Strum crate, we cheat a little bit to display enum values.
#[derive(Copy, Clone, Debug, PartialEq, strum_macros::Display)]
pub enum MenuMessages {
    Home,
    About,
    HowItStarted,
    Macros,
    SoWhat,
    Books,
    Resources,
}

//--------------------------------------------------------------------------------------------------
// And we need to keep track of the state of our menu
pub(crate) struct RootComponent {
    pub active_item: MenuMessages,
}

//--------------------------------------------------------------------------------------------------
impl RootComponent {
    //----------------------------------------------------------------------------------------------
    // Rendering the main menu. Yes, it's messy by design, you'll see... I think?
    pub fn render_main_menu(&self, link: &Scope<RootComponent>) -> Html {
        return html! {
            <div id="menu">
                <div class="pure-menu">
                    <a class="pure-menu-heading" href="/">
                        <img src="/images/snaplet.png" class="logo-img" alt="Logo"/>
                    </a>
                    <ul class="pure-menu-list">
                        if self.active_item == MenuMessages::Home {
                            <li class="pure-menu-item pure-menu-selected">
                                <a href="#" class="pure-menu-link"
                                    onclick={link.callback(|_| MenuMessages::Home)}>{"Home"}</a>
                            </li>
                        } else {
                            <li class="pure-menu-item">
                                <a href="#" class="pure-menu-link"
                                    onclick={link.callback(|_| MenuMessages::Home)}>{"Home"}</a>
                            </li>
                        }
                        if self.active_item == MenuMessages::HowItStarted {
                            <li class="pure-menu-item pure-menu-selected">
                                <a href="#" class="pure-menu-link"
                                    onclick={link.callback(|_| MenuMessages::HowItStarted)}>{"How It Started"}</a>
                            </li>
                        } else {
                            <li class="pure-menu-item">
                                <a href="#" class="pure-menu-link"
                                    onclick={link.callback(|_| MenuMessages::HowItStarted)}>{"How It Started"}</a>
                            </li>
                        }
                        // That's tedious!
                        // Let's cheat a bit with macros...
                        if self.active_item == MenuMessages::About {
                            {menu_link_active!("About", link.callback(|_| MenuMessages::About))}
                        } else {
                            {menu_link_inactive!("About", link.callback(|_| MenuMessages::About))}
                        }
                        if self.active_item == MenuMessages::SoWhat {
                            {menu_link_active!("So What", link.callback(|_| MenuMessages::SoWhat))}
                        } else {
                            {menu_link_inactive!("So What", link.callback(|_| MenuMessages::SoWhat))}
                        }
                        // That's still messy, so how about more cheating!
                        {menu_link!("Macros",
                                    link.callback(|_| MenuMessages::Macros),
                                    self.active_item == MenuMessages::Macros)}
                        {menu_link!("Books",
                                    link.callback(|_| MenuMessages::Books),
                                    self.active_item == MenuMessages::Books)}
                        {menu_link!("Resources",
                                    link.callback(|_| MenuMessages::Resources),
                                    self.active_item == MenuMessages::Resources)}
                    </ul>
                </div>
            </div>
        };
    }
    //----------------------------------------------------------------------------------------------
    pub fn switch_to_selected_content(&self, active_item: MenuMessages) -> Html {
        return match active_item {
            MenuMessages::About => self.render_about(),
            MenuMessages::Home => self.render_home(),
            MenuMessages::Books => self.render_books(),
            MenuMessages::SoWhat => self.render_so_what(),
            MenuMessages::Macros => self.render_macros(),
            MenuMessages::Resources => self.render_resources(),
            MenuMessages::HowItStarted => self.render_how_it_started(),
            _ => self.render_error(active_item),
        };
    }

    //----------------------------------------------------------------------------------------------
    pub fn render_error(&self, active_item: MenuMessages) -> Html {
        return html! {
            <section class="section">
                <div class="container">
                    <div id="main">
                        <div class="header">
                            <h1>{"Unknown Page Requested"}</h1>
                            <h2>{"\""}{active_item}{"\" is unknown."}</h2>
                        </div>
                    </div>
                </div>
            </section>
        };
    }

    //----------------------------------------------------------------------------------------------
    pub fn render_home(&self) -> Html {
        return html! {
            <>
            <section class="section">
                <div class="container">
                    <div id="main">
                        <div class="header">
                            <h1>{"Rust, WebAssembly and Two Smoking Barrels"}</h1>
                            <h2>{"What lies beyond Hello World? Do Yew know?"}</h2>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section">
                <div class="container">
                    <div class="content">
                        <p style="text-align: center; font-weight: bold;">
                            {"Please consider supporting animal rescue"}
                            <br/>
                            <img src="/images/daw_logo_edit.png"/>
                        </p>
                        <p style="text-align: center; font-size: 36pt;">
                            <a href="https://deadanimalswalking.co.za/" rel="noreferrer noopener" target="_blank">
                                {"https://deadanimalswalking.co.za/"}
                            </a>
                        </p>
                    </div>
                </div>
            </section>
            </>
        };
    }

    //----------------------------------------------------------------------------------------------
    pub fn render_how_it_started(&self) -> Html {
        return html! {
            <>
                {page_header!("How I got started", "Actual footage of my first journey into Rust")}
                {page_content!(html!{
                    <>
                        <p style="text-align: center;">
                            {"Yes, that's me, in my only car. Really want to take advice from me?"}
                            <br/>
                            <br/>
                            <video width="1024" controls=true>
                                <source src="images/jimny_devconf.mp4" type="video/mp4" />
                            </video>
                        </p>
                    </>
                })}
            </>
        };
    }

    //----------------------------------------------------------------------------------------------
    pub fn render_about(&self) -> Html {
        return html! {
            <>
            {page_header!("About Now", "Is where you can run and still get to a real talk!")}
            {page_content!(html!{
                <>
                    <h2>{"This talk is sort of technical and assumes you have basic Rust knowledge."}</h2>
                    <p>{"We'll look into Rust, some WebAssembly concepts and a framework called Yew. \
                    It might be a bit hard to follow if you don't have basic Rust knowledge, but I've \
                    been informed that folks with JavaScript and/or React knowledge will know what's \
                    going on most of the time as well."}</p>
                    <p>{"The general plan is to run through a bit of code, show you what it does, \
                    maybe change a thing or two and then open the floor for a bit of Q&A."}</p>
                    <p>{"I work at Snaplet and you should visit "}
                        <a href="https://www.snaplet.dev/">
                            {"https://www.snaplet.dev/"}
                        </a>{" to check it out. Other than that, I'm all over the internet, \
                        got all them Twitters and things, so if you want to find me, you can."}
                    </p>
                    <br/>
                    <p style="text-align: center;">
                        {render_snappy_image!()}
                    </p>
                    <br/>
                    <hr/>
                    <p style="text-align: center; font-size: larger; font-weight: bold;">
                        {"Stand a chance to win a prize, visit "}
                        <a href="https://www.devconf.co.za/rate/">
                            {"https://www.devconf.co.za/rate/"}
                        </a>
                    </p>
                    <hr/>
                </>
            })}
            </>
        };
    }

    //----------------------------------------------------------------------------------------------
    pub fn render_books(&self) -> Html {
        return html! {
            <>
            {page_header!("Books", "I've found these rather useful.")}
            {page_content!(html!{
                <>
                    {"There's an absolute wealth of Rust information available on the official \
                    website of course, but sometimes, we need just a little bit more. I've found \
                    these books rather useful."}
                    <br/>
                    <hr/>
                    <h3>{"Hands-on Rust"}</h3>
                    {"By Herbert Wolverson"}
                    <br/>
                    <p>{"Learn Rust by writing your own games. Lots of fun mixed with building serious \
                    Rust skills.  If you aren't sure where to start, this is the one to get."}</p>
                    {"Find it at "}
                    <a href="https://pragprog.com/titles/hwrust/hands-on-rust/">
                        {"https://pragprog.com/titles/hwrust/hands-on-rust/"}
                    </a>
                    <br/>
                    <hr/>
                    <h3>{"Zero To Production In Rust"}</h3>
                    {"By Luca Palmieri"}
                    <br/>
                    <p>{"Excellent guide to build production systems in Rust. This book really takes you \
                    from knowing nothing to deploying production-grade software. It's a journey!"}</p>
                    {"Find it at "}
                    <a href="https://www.zero2prod.com/">
                        {"https://www.zero2prod.com/"}
                    </a>
                </>
            })}
            </>
        };
    }

    //----------------------------------------------------------------------------------------------
    pub fn render_macros(&self) -> Html {
        return html! {
            <>
            {page_header!("Macros", "Meta programming for mortals.")}
            {page_content!(html!{
                <>
                    <p>{"When I first saw macros, I kind of glanced over them and moved along \
                    swiftly. Mostly because of my C++ experiences!  But wow, when I \
                    started doing web development work in Rust, I gave them a second shot, and \
                    well, ok!"}</p>
                    <p style="text-align: center;">
                        <a href="https://doc.rust-lang.org/rust-by-example/macros.html">
                            {"https://doc.rust-lang.org/rust-by-example/macros.html"}
                        </a>
                        <br/>
                        <br/>
                        <img src="/images/vince_macros.jpeg" width="40%"/>
                    </p>
                </>
            })}
            </>
        };
    }

    //----------------------------------------------------------------------------------------------
    pub fn render_resources(&self) -> Html {
        return html! {
            <>
            {page_header!("Resources", "Things to help you!")}
            {page_content!(html!{
                <>
                    <p>{"Links! More links! All the links!"}</p>
                    <ul style="list-style: none;">
                        <br/>
                        <hr/>
                        <b>{"General Rust"}</b>
                        {resource_link!("Official Rust Website", "https://www.rust-lang.org/")}
                        {resource_link!("Official Rust and Wasm documentation", "https://rustwasm.github.io/docs/book/")}
                        {resource_link!("Learn Rust by Example", "https://doc.rust-lang.org/rust-by-example/index.html")}
                        <br/>
                        <hr/>
                        <b>{"Yew"}</b>
                        {resource_link!("Official Yew website", "https://yew.rs/")}
                        {resource_link!("Awesome Yew projects", "https://yew.rs/ja/community/awesome")}
                        {resource_link!("Trunk", "https://trunkrs.dev/")}
                    <br/>
                    <br/>
                    </ul>
                    <hr/>
                    <p style="text-align: center; font-size: larger; font-weight: bold;">
                        {"Stand a chance to win a prize, visit "}
                        <a href="https://www.devconf.co.za/rate/">
                            {"https://www.devconf.co.za/rate/"}
                        </a>
                    </p>
                    <hr/>
                    <br/>
                    <p style="text-align: center;">
                        {"Also..."}
                        <h1 style="font-size: 128pt;">{"Q & A"}</h1>
                    </p>
                </>
            })}
            </>
        };
    }
}
